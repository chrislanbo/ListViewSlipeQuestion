package com.dameng.listview;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ListTestActivity extends Activity {

	private ListView mListView;
	private MyAdapter mAdapter;
	private List<String> mList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initData();
		initView();
	}

	private void initData() {
		mList = new ArrayList<String>();
		for (int i = 0; i < 100; i++) {
			mList.add("这是第"+i+"条数据");
		}
	}

	private void initView() {
		mListView = (ListView)findViewById(R.id.lv);
		mAdapter = new MyAdapter();
		mListView.setAdapter(mAdapter);
	}
	class MyAdapter extends BaseAdapter{

		@Override
		public int getCount() {
			return mList != null ? mList.size() : 0;
		}

		@Override
		public Object getItem(int position) {
			return mList != null ? mList.get(position) : null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if(convertView==null){
				convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, null);
				holder = new ViewHolder();
				holder.mTextView = (TextView) convertView.findViewById(R.id.tv);
				convertView.setTag(holder);
				Log.i("test", "Set");
			}else{
				
				Log.i("test", "    Get");
				holder = (ViewHolder) convertView.getTag();
			}
//			????
			if(position>=getCount()){
				Log.e("test", "大于");
				return convertView;
			}
			Log.i("test", "position= " + position);
			holder.mTextView.setText(mList.get(position));
			return convertView;
		}
		
	}
	class ViewHolder{
		TextView mTextView;
	}

}
